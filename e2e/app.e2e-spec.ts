import { PcmAdminPage } from './app.po';

describe('pcm-admin App', () => {
  let page: PcmAdminPage;

  beforeEach(() => {
    page = new PcmAdminPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
