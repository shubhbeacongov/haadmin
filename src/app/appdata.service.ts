import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AppdataService {
      sampleDate: any;
      fullDate:any;
      calenderShow=false;
      departObj: any; 
      objIndex:any;
      userData:any; 
      updateAttendance:any;
      lodingIconShow = false;
      finalSubmit = [];
      errorMsg:String;
      errorDiv = false;
   
  constructor(public http: Http) {
      this.dafualtDate();
      this.sampleDate=this.fullDate;
      this.userData = {"username":'' , "password":'', "userRes":''};
      console.log("shubh userData", this.userData.userRes);
      
  }
  
    //ip= "http://192.168.185.154:8080";
    
    //production server
     //ip="http://192.168.191.10:8080/HAAppDashboard/";

    //local test Server
    ip="http://172.20.40.197:9090/issueTracker/";
   //ip="http://172.20.20.216:9090/DashBoard/";

dafualtDate(){
      var date= new Date();
      var day=date.getDate();
      var month= date.getMonth()+1;
      var year = date.getFullYear();
      this.fullDate= day + "-" + month + "-" + year; 
  }
      
     public addAttendanceDept=[];
      
     showAlert(eMsg) {
        this.errorMsg = eMsg;
         this.errorDiv = true;
         console.log("Message Show", eMsg);
        
        if(eMsg.status == 0) {
            this.errorMsg = "Server not reachable, Please check network";
             console.log("0", this.errorMsg);  
         }
         else if(eMsg.status == 401) {
            this.errorMsg = "Invalid Credentials";
            console.log("401", this.errorMsg);
         }
         else if(eMsg.status == 403) {
            this.errorMsg = "Forbidden Error";
            console.log("403", this.errorMsg);
         }
         else if(eMsg.status == 404) {
            this.errorMsg = "No Data Found";
            console.log("404", this.errorMsg);
         }
         setTimeout(()=>{this.errorMsg = ""; this.errorDiv = false;}, 2000);
     }
     
     autoClose() {
        this.errorMsg = "";
        this.errorDiv = false;
     }
   
   
/*      errorHandle(pErr) {  
         this.alertMsg = "Something went wrong";
         console.log("Something went wrong", pErr);
         if(pErr.status == 0) {
            this.alertMsg = "Server not reachable, Please check network";
             console.log("0", this.alertMsg);  
         }
         else if(pErr.status == 401) {
            this.alertMsg = "Invalid Credentials";
            console.log("401", this.alertMsg);
         }
         else if(pErr.status == 403) {
            this.alertMsg = "Forbidden Error";
            console.log("403", this.alertMsg);
         }
         else if(pErr.status == 404) {
            this.alertMsg = "No Data Found";
            console.log("404", this.alertMsg);
         }
         setTimeout(()=>{this.alertMsg = ''; this.errorHandleDIv = false;}, 2000);
         
      }*/
      
}