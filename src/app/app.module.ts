import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/map';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { AppdataService } from './appdata.service';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AttendanceComponent } from './attendance/attendance.component';

import { RouterModule, Routes } from '@angular/router';

import { NgIdleKeepaliveModule } from '@ng-idle/keepalive'; // this includes the core NgIdleModule but includes keepalive providers for easy wireup
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting
import {NgIdleModule} from '@ng-idle/core';
import { IssuelogComponent } from './issuelog/issuelog.component';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'attendance', component: AttendanceComponent },
  { path: 'Issuelog', component: IssuelogComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AttendanceComponent,
    IssuelogComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MomentModule,
    NgIdleKeepaliveModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
