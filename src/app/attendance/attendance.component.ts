import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { AppdataService } from '../appdata.service';


@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
  attendanceData:any;
  attendenceDataArr:Array<any>=[];
  attendanceDept:any;
  attendencShift:any;
  attendanceDate:any;
  splitDate:any;
  docId:any;
  repeatedDoctor:any;
  inTime:any;
  outTime:any;  
  deptName:any;
  deptId:any;
  submitDate:any;
  Shift:any;
  finalRawData:Array<any>=[];
  errorMsg:String;  
  designationToShow:any;

constructor(public http: Http, public appdataService:AppdataService) { }

  ngOnInit() {
  }
  
getAttendanceData(pDept, pShift, pDate){
if(pDept!=undefined || pDept!=null || pDate!=undefined || pDate!=null || pDate!=undefined || pDate!=null){

  this.appdataService.lodingIconShow = true;
  
console.log("pDept",pDept);
console.log("pShift",pShift);
console.log("pDate",pDate);
this.splitDate=pDate.split("-");
this.attendanceDate=this.splitDate[1] + "-" + this.splitDate[2] + "-" + this.splitDate[0];
  this.submitDate=this.splitDate[2] + "-" + this.splitDate[1] + "-" + this.splitDate[0];
  console.log("this.submitDate",this.submitDate);
console.log("this. attendanceDate",this. attendanceDate)

  let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username + ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});    
          console.log("shubh vHeaders",vHeaders);    
          
          
          let vOptions = new RequestOptions({"headers": vHeaders});
          console.log("shubh vOptions", vOptions);
           this.attendanceData=[];
           this.attendenceDataArr=[];
	this.http.get(this.appdataService.ip+'attendence/webUi?date='+this.attendanceDate+'&deptId='+pDept+'&shiftId='+pShift+'', {"headers": vHeaders})
          .map(res => res.json()).subscribe(res=> {
            this.attendanceData = res;
                  console.log("Attendance data", this.attendanceData);  
             for(var i=0; i<this.attendanceData.length; i++) {
               this.deptId=this.attendanceData[i].deptId;
      	this.attendanceData[i]['selfIndex'] = i;
      	for(var j=0; j<this.attendanceData[i].day.length; j++) {
      		this.attendanceData[i].day[j]['selfIndex'] = j;
      		for(var k=0; k<this.attendanceData[i].day[j].shifts.length; k++) {
      			this.attendanceData[i].day[j].shifts[k]['selfIndex'] = k;
      			for(var l=0; l<this.attendanceData[i].day[j].shifts[k].rooms.length; l++) {
                  this.Shift=this.attendanceData[i].day[j].shifts;
      				for(var m=0; m<this.attendanceData[i].day[j].shifts[k].rooms[l].doctors.length; m++) {
  this.attendenceDataArr.push({"roomId":this.attendanceData[i].day[j].shifts[k].rooms[l].roomId,"roomName":this.attendanceData[i].day[j].shifts[k].rooms[l].roomName, "signIn": '', "signOut": '', "designation":this.attendanceData[i].day[j].shifts[k].rooms[l].doctors[m].designation, "doctorName":this.attendanceData[i].day[j].shifts[k].rooms[l].doctors[m].doctorName, "docId":this.attendanceData[i].day[j].shifts[k].rooms[l].doctors[m].docId, "doctors":this.attendanceData[i].doctorsInDepartment});
      				
                    }
      			}
      		}
      	}
      }
        
       console.log("attendenceDataArr",this.attendenceDataArr);     
      
        this.appdataService.lodingIconShow = false;    
      },err=>{
        this.appdataService.showAlert(err);
        console.log("err", err);
        this.appdataService.lodingIconShow = false;
      }); 


    }else {
        this.appdataService.showAlert("Select data");
    }

  
}
      
     addRowInAttendance(pItem,pIndex){
     let newItem=Object.assign({},pItem);
     newItem.doctorName="";
     newItem.docId=null;
       newItem.signIn=null;
         newItem.signOut=null;
  console.log("pIndex",pIndex);
  console.log("pItem",pItem);
  this.attendenceDataArr.splice(pIndex+1,0,newItem);
       
  this.finalRawData = this.attendenceDataArr;
  console.log("this.attendenceDataArrnew",this.attendenceDataArr);
  

}
  
  deleteRowAttendence(dIndex){

    this.attendenceDataArr.splice(dIndex,1);
     }
submitData:any;
getRoomIndexOfSubmitData(pRoomName){
for(var i=0; i<this.submitData[0].shifts[0].rooms.length; i++){
if(this.submitData[0].shifts[0].rooms[i].roomName==pRoomName){
      return i;
    }
  }
  return -1;
}
    finalSubmitData() {
      this.appdataService.lodingIconShow = true;
      
      for(var a=0; a<this.appdataService.addAttendanceDept.length; a++){
        console.log("this.appdataService.addAttendanceDept.length", this.appdataService.addAttendanceDept);
        if(this.appdataService.addAttendanceDept[a].dept_id==this.deptId){
          this.deptName=this.appdataService.addAttendanceDept[a].dept_name_eng;
          console.log("this.deptName",this.deptName);
        }
      }
      
      
    console.log("finalSubmitData");
    this.submitData=[
    {
      "deptNm":   this.deptName,
      "deptId": this.deptId,
      "pcmName": this.appdataService.userData.username,
      "pcmid": 2,
      "date": this.submitDate,
      "shifts": [
                  {
                    "shiftId":this.attendencShift,
                    "rooms":[]
                  }
                ]
    }
    ]
    console.log("this.submitData", this.submitData);
      
    for(var i=0; i<this.attendenceDataArr.length; i++){
      var vIndexFound=this.getRoomIndexOfSubmitData(this.attendenceDataArr[i].roomName);
      if(vIndexFound<0){
        var vRoom={
          roomName:this.attendenceDataArr[i].roomName,
          "roomId":this.attendenceDataArr[i].roomId,
            doctors:[
              {    "designation": this.attendenceDataArr[i].designation,
                "docId": this.attendenceDataArr[i].docId,
                "doctorName":this.attendenceDataArr[i].doctorName,
                "signIn": this.attendenceDataArr[i].signIn,
              "signOut": this.attendenceDataArr[i].signOut
              }
            ]
          };
        this.submitData[0].shifts[0].rooms.push(vRoom);
          console.log("vRoom", vRoom);
          console.log("this.submitData.shifts", this.submitData[0].shifts);
      }else{
          let vDoc={
                   "designation": this.attendenceDataArr[i].designation,
                  "docId": this.attendenceDataArr[i].docId,
                  "doctorName":this.attendenceDataArr[i].docName,
                  "signIn": this.attendenceDataArr[i].signIn,
                  "signOut": this.attendenceDataArr[i].signOut
                };
         this.submitData[0].shifts[0].rooms[vIndexFound].doctors.push(vDoc);
          console.log("this.submitData", this.submitData); 
          console.log("vIndexFound", vIndexFound); 
          console.log("vDoc", vDoc); 

        }
      }
      
      let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username + ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});        
          
      let vOptions = new RequestOptions({"headers": vHeaders});
            
    this.http.post(this.appdataService.ip+"/attendence", this.submitData, {"headers": vHeaders})
      .subscribe(res => { 
              var vRes:any;
                vRes=res;
                console.log("vRes",vRes);
                console.log("vRes._body.indexOf",vRes._body.indexOf("errorCode"));
                 if(vRes._body.indexOf("errorCode")>0){
                 console.log("errorCode=412");
                  var errJson=JSON.parse(vRes._body);
                  console.log("errJson=",errJson);
                 this.errorMsg = "Duplicate Data Entry";
                alert("Duplicate Data Entry");
                 this.appdataService.lodingIconShow = false;
        }
         
        else if(res.statusText.indexOf("Created")==0){
          this.errorMsg = "Data Submit Successfull";
           
       
          
            this.errorMsg = "Data Submit Successfull";
            alert("Data Submit Successfull");
        
        console.log("res",res);
        this.appdataService.lodingIconShow = false;
        }
     },(err) => {
        this.errorMsg = "Data Not Submitted"
        alert("Try Again");
        console.log("err",err);
       this.appdataService.lodingIconShow = false;
    });
      console.log("submitData", JSON.stringify(this.submitData));
  }

      getConsultant(p){
         this.designationToShow=p;
         console.log("this.designationToShow",this.designationToShow);
      }
      
  }

      

