import { Component, OnInit } from '@angular/core';

import { RouterModule, Router } from '@angular/router';
import { AppdataService } from '../appdata.service';

import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginData:any;
  username:any;
  password:any;
  Error:String;
  
constructor(private route: Router, public http: Http, private appdataService:AppdataService) { 
  this.username="";
  this.password="";
}

  ngOnInit() {
  }
  

	signIn(pUsername, pPassword){
    if(pUsername == "" || pPassword == ""){
        this.Error = "Please Fill Credentials";
        setTimeout(()=>{this.Error = "";}, 1000);
    }
    else{
           this.appdataService.lodingIconShow = true;
        
      
          let vHeaders=new Headers({"Authorization": "Basic " + btoa(pUsername+ ":" + pPassword), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});    
          /*console.log("shubh vHeaders",vHeaders);    
          console.log("shubh username",pUsername);    
          console.log("shubh password",pPassword); */   
          
          let vOptions = new RequestOptions({"headers": vHeaders});
          /*console.log("shubh vOptions", vOptions);*/
          
          
         /* this.http.get('../assets/loginData.json')*/
           this.http.get(this.appdataService.ip + 'attendence/search', {"headers": vHeaders})
              .map(res => res.json()).subscribe(res=> {
                this.loginData = res;
                console.log("login Data", this.loginData);
                /*console.log("log data", JSON.stringify(this.loginData));*/
    
                this.appdataService.userData.userRes = this.loginData; 
                this.appdataService.userData.username = pUsername; 
                this.appdataService.userData.password = pPassword;
                
                this.route.navigateByUrl("/home");
                this.appdataService.lodingIconShow = false;
              },err=>{
                    console.log("err", err.status);
                    this.appdataService.showAlert(err);
                    /*this.Error = "Please provide correct username and password";
                    setTimeout(()=>{this.Error = "";}, 1000);*/
                    this.appdataService.lodingIconShow = false;
              }); 
          

      }
         
           
      }
  
    fillDataForDemo() {
        this.username = "username1"; 
        this.password = "password123";
    }
    
    
    forget() {
        this.Error = "Contact to Admin Team";
        setTimeout(()=>{this.Error = "";}, 1000);
    }
  
  }