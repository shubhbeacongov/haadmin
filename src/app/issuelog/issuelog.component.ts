import { Component, OnInit } from '@angular/core';
import { Http, Headers,RequestOptions } from '@angular/http';
import { AppdataService } from '../appdata.service';
import {Pipe, PipeTransform} from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-issuelog',
  templateUrl: './issuelog.component.html',
  styleUrls: ['./issuelog.component.css']
})
export class IssuelogComponent implements OnInit {
category:any;
subCategory:any;
location:any;
preciseLocation:any;
actionTaken:any;
issueSummaryD:any; 
raisedTo:any; 
issueCreateUi:any;
IssueFilterMode = false;

filterCategory="";
filterSubCategory="";
filterRaisedTo="";
filterLocation="";
filterSummary="";
filterActionTaken="";

  
  
  
constructor(private http:Http, private appdataService:AppdataService) { 
    this.getIssueList();
  this.getIssueUi();
  }
  
issueList=[];
  
  ngOnInit() {  
  }
  
getIssueList(){
  
  this.appdataService.lodingIconShow = true;
      
  let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});
  console.log("vHeaders",vHeaders);
      
  let vOptions = new RequestOptions({"headers": vHeaders});
  console.log("vOptions",vOptions);
  
  this.http.get( this.appdataService.ip + "HAapp/issueList",{"headers": vHeaders}).map(res => res.json()).subscribe(res=>{
    this.issueList=res;    
    console.log("this.issueList",this.issueList);
    this.appdataService.lodingIconShow = false;
  },err=>{
    this.appdataService.showAlert(err);
    console.log("err", err);
    this.appdataService.lodingIconShow = false;
    
  });
}
  
  
  
   getIssueUi(){

    this.appdataService.lodingIconShow = true;
      
  let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});
  console.log("vHeaders",vHeaders);
      
  let vOptions = new RequestOptions({"headers": vHeaders});
  console.log("vOptions",vOptions);
  
  this.http.get(this.appdataService.ip + "HAapp/issue/ui",{"headers": vHeaders}).map(res => res.json()).subscribe(res=>{
    this.issueCreateUi=res;  
    console.log("this.issueCreateUi",this.issueCreateUi);
    this.appdataService.lodingIconShow = false;
  },err=>{
    this.appdataService.lodingIconShow = false;
    console.log("err", err);
  });
}
  
  
editIssue(pIssue){
 
  console.log("pIssue",pIssue);
  this.category= pIssue.category.categoryId;
  this.prepareSubCategory(this.category);
  this.subCategory= pIssue.subCategory.sCategoryId;
  this.location=pIssue.location.locName;
  this.preciseLocation=pIssue.precisLocation;
  this.actionTaken=pIssue.action;
  this.issueSummaryD=pIssue.summary;
  this.raisedTo=pIssue.raisedTo.uName; 
  
  /*console.log("category", this.category);
  console.log("subCategory",  this.subCategory);
  console.log("location",  this.location);
  console.log("preciseLocation", this.preciseLocation);
  console.log("this.actionTaken", this.actionTaken);
  console.log("this.issueSummaryD", this.issueSummaryD);
  console.log("this.raisedTo", this.raisedTo);*/
}
  
    
  selectedSubCategories=[];

  prepareSubCategory(pIndex) {
      for(var i=0; i<this.issueCreateUi.Categories.length; i++){
        console.log("shubh length", this.issueCreateUi.Categories.length);
        if(this.issueCreateUi.Categories[i].categoryId ==  this.category){
            this.selectedSubCategories = this.issueCreateUi.Categories[i].sCategories;
            console.log("shubh selectedSubCategories", this.selectedSubCategories); 
          }

        } 
    }

  
  
    issueFilterData(pItem) {
      console.log("shubh IssueFilter", pItem);
      
      console.log("filterCategory", this.filterCategory);
      console.log("filterSubCategory", this.filterSubCategory);
      console.log("filterRaisedTo", this.filterRaisedTo);
      console.log("filterLocation", this.filterLocation);
      console.log("filterSummary", this.filterSummary);
      console.log("filterActionTaken", this.filterActionTaken);
  
      if(this.IssueFilterMode) {
        let sCategory=true, sSubCategory=true, sRaisedTo=true, sLocation=true, sSummary=true, sActionTaker=true;
        
        if(this.filterCategory!=""){
if(pItem.category.categoryName.toUpperCase().indexOf(this.filterCategory.toUpperCase())!=0){
            sCategory=false;
          }
        }
        
        if(this.filterSubCategory!=""){
          if(pItem.subCategory.sCategoryName.indexOf(this.filterSubCategory)!=0){
            sSubCategory=false;
          }
        }
        
        if(this.filterRaisedTo!=""){
          if(pItem.raisedTo.uName.indexOf(this.filterRaisedTo)!=0){
            sRaisedTo=false;
          }
        }
        
        if(this.filterLocation!=""){
          if(pItem.location.locName.indexOf(this.filterLocation)!=0){
            sLocation=false;
          }
        }
        
        /*if(this.filterSummary!=""){
          if(pItem.summary.indexOf(this.filterSummary)!=0){
            sSummary=false;
          }
        }
        
        if(this.filterActionTaken!=""){
          if(pItem.action.indexOf(this.filterActionTaken)!=0){
            sActionTaker=false;
          }
        }*/
        
        return (sCategory && sSubCategory && sRaisedTo && sLocation && sSummary && sActionTaker);
        
      }else {
        return true;
      }
    }
  
}
