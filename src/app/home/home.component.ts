import { Component, OnInit } from '@angular/core';
import { Http, Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';


import { RouterModule, Router } from '@angular/router';
import { AppdataService } from '../appdata.service';
import {Pipe, PipeTransform} from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],



})
export class HomeComponent implements OnInit {
    
    deptObj:any;
    doctorName:any;
    roomNo:any;
    department:any;
    intime:any;
    outTime:any;
    date:any;
    pcmName:any;
    tableHeader= ["Doctor", "Room", "Dept", "In", "Out", "Date", "PCM Name"];
    tableHeaderData = 0;
    headerInput = false;
    selectedItemIndex:any;
    filterMode=false;
    filterPCM="";
  filterDate="";
  filterDept="";
  filterRoom="";
  filterDoctor="";
  
constructor(private route: Router, public appdataService:AppdataService, public http: Http) { }

  ngOnInit() {
  }
  
rowFilter(pData){
  console.log("Shubh rowFilter", pData);
  if(this.filterMode){
    let vDoctor=true, vRoom=true, vDept=true, vDate=true, vPCM=true;
    
  if(this.filterDoctor!=""){
    if(pData.doctorname.toUpperCase().indexOf(this.filterDoctor.toUpperCase())!=0){
      vDoctor=false;
    }
  }
  if(this.filterRoom!=""){
    if(pData.roomno.toUpperCase().indexOf(this.filterRoom.toUpperCase())!=0){
      vRoom=false;
    }
  }
  if(this.filterDept!=""){
    if(pData.department.toUpperCase().indexOf(this.filterDept.toUpperCase())!=0){
      vDept=false;
    }
  }
  if(this.filterDate!=""){
    if(pData.date.toUpperCase().indexOf(this.filterDate.toUpperCase())!=0){
      vDate=false;
    }
  }
  if(this.filterPCM!=""){
    if(pData.pcmname.toUpperCase().indexOf(this.filterPCM.toUpperCase())!=0){
      vPCM=false;
    }
  }
    
    return(vDoctor && vRoom && vDept && vDate && vPCM);
  }
  else{
    return true;
  }
}
    
 /* HeaderOptionSelect(pindex) {
    this.tableHeaderData = pindex;
    console.log("Header Index", pindex);
  }
  
  OpenInput(pItem) {
  if(this.headerInput == true){
    this.headerInput = false;
  }
for(var i=0; i<this.tableHeader.length; i++){
if(this.tableHeader[i]==pItem) {
  this.headerInput = true;
  console.log("this.headerInput",this.headerInput);
}
}
     
   
    
  }
  */
    editAttendance(pDept, pIndex) {
      console.log("pIndex",  pIndex);
         
      this.appdataService.objIndex=pIndex;
        
          this.deptObj=pDept;
          
          this.doctorName = pDept.doctorname;
          this.roomNo= pDept.roomno;
          this.department = pDept.department;
          this.intime = pDept.singin;
          this.outTime = pDept.signout;
          this.date  = pDept.date;
          this.pcmName= pDept.pcmname;
         console.log("pDept", pDept);
          
    }
    
    saveAttendance(pDocName, pRoomNo, pDepartment, pIntime, pOutTime, pDate, pPcmName) {
      this.appdataService.lodingIconShow = true;
      
      let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});
      console.log("vHeaders",vHeaders);
      
      let vOptions = new RequestOptions({"headers": vHeaders});
      console.log("vOptions",vOptions);
          var vBody= {
                  "roomno": pRoomNo,
                  "designation": "Professor",
                  "doctorname": pDocName,
                  "singin": pIntime,
                  "signout": pOutTime,
                  "department": pDepartment,
                  "date": pDate,
                  "pcmname": pPcmName,
                  "doc_id": 107,
                  "shift_id": 1,
                  "room_id": 233,
                  "dept_id": 25,
                  "pcm_id": 2,
                  "serial_id": 351,
                  "created_on": null,
                  "modified_on": null,
                  "createddate": "2017-06-14",
                  "modifiedDate": null
          };
         this.http.put(this.appdataService.ip + 'attendence', vBody,  {"headers": vHeaders})
          .map(res => res.json()).subscribe(res=> {
            this.appdataService.updateAttendance = res;
             this.appdataService.userData.userRes[this.appdataService.objIndex]=this.appdataService.updateAttendance;
            console.log("updateAttendance", this.appdataService.updateAttendance);
            /*console.log("log data", JSON.stringify(this.loginData));*/
            this.appdataService.lodingIconShow = false;
            
      },err=>{
        this.appdataService.showAlert(err);
         console.log("err", err);
        this.appdataService.lodingIconShow = false;
      }); 
    /* this.appdataService.loginData[this.appdataService.objIndex]['doctorname']=pDocName;
  console.log("Docname",this.appdataService.loginData[ this.appdataService.objIndex].doctorname);*/
      /*this.deptObj.roomno=pRoomNo;
      this.deptObj.department=pDepartment;
      this.deptObj.singin=pIntime;
      this.deptObj.signout=pOuttime;
      this.deptObj.date=pDate;
      this.deptObj.pcmname=pPcmName;*/
    }
  
}
