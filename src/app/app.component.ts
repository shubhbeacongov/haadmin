import { Component } from '@angular/core';
import { AppdataService } from './appdata.service';
import { RouterModule, Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
   providers: [AppdataService]
})
export class AppComponent {
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  title = 'app';
  dateDivShow: any;
  roomData = []; 
  searchResponseData:any;
  doctorsDepartName = [];
  deptValue:0;
  roomValue:any;
  doctorValue:any;
  toDateValue:any;
  fromDateValue:any;
  arrayOfSampleDate:any;


constructor(private idle: Idle, private keepalive: Keepalive, public appdataService: AppdataService, private route: Router,  public http: Http) {

}
  
  
  
 
 
getCalender(){
  this.route.navigateByUrl("/home");
 if(this.appdataService.calenderShow==true){
    this.appdataService.calenderShow=false;
 }else{
  this.appdataService.calenderShow=true;
  console.log("calnderflag", this.appdataService.calenderShow);
 }

}


    loadYear="2017";
    selectedIndex = -1; 
    loadMonth:any;
    SelectedMonthIndex: any;
    date:any;
    monthArray  = [
                            {
                            "month":"January",
                              "index":1
                            }, 
                            {
                            "month":"February",
                              "index":2
                            },
                            {
                            "month":"March",
                              "index":3
                            
                           },
                             {
                             "month":"April",
                              "index":4
                            
                           },
                             {
                             "month":"May",
                              "index":5
                            
                           },
                            
                             {
                             "month":"June",
                              "index":6
                            
                             },
                            
                             {
                             "month":"July",
                              "index":7
                            
                             },
                            
                              {
                              "month":"August",
                              "index":8
                            
                             },
                            
                              {
                              "month":"September",
                              "index":9
                            
                             },
                              {
                              "month":"october",
                              "index":10
                            
                             },
                              {
                              "month":"November",
                              "index":11
                            
                             },
                              {
                              "month":"December",
                              "index":12
                            
                             }
                             
                        ];
yearArray : Array<string> = [
                            "2017",
                            "2018", 
                            "2019", 
                            "2020"
                        ];  
                       
dateArray = [];
   v: any;                         
                                
                        
   
   
  /* getYear(pYear){
    this.loadYear=pYear;
    this.showDiv="month";
    
   }*/
    
   getDay(pIndex,pYear,pMonth){
    console.log("index",pIndex);
    console.log("year",pYear);
   
    this.loadMonth=pMonth;
    this.SelectedMonthIndex = pIndex;
    /*console.log("this.SelectedMonthIndex",this.SelectedMonthIndex);
    console.log("loadMonth",this.loadMonth);*/
        this.v=new Date(pYear,pIndex, 0);
        console.log("this.v", this.v);
       this.dateArray=[];
          for(var i=1; i<=this.v.getDate(); i++){
            this.dateArray.push(i);
            /*console.log("dateArray", this.dateArray);*/
           }

   }
     showDiv(index) {
    this.selectedIndex = index;
    /*console.log("this.selectedIndex", this.selectedIndex);
    console.log("this.appdataService.sampleDate", this.appdataService.sampleDate);*/
      
    /*this.arrayOfSampleDate = this.appdataService.sampleDate.split("-");
    console.log("arrayOfSampleDate", this.arrayOfSampleDate);
      console.log("arrayOfSampleDate 0", this.arrayOfSampleDate[0]);
      console.log("arrayOfSampleDate 1", this.arrayOfSampleDate[1]);
      console.log("arrayOfSampleDate 2", this.arrayOfSampleDate[2]);*/
  }
    
   /*goMonth(){
    this.showDiv="month";
    }
    goYear(){
     this.showDiv="year";
    }*/
    showYear(pYear){
    this.loadYear=pYear;
    }
  ngOnInit() {
  
 }
  getDate(pDate){
  this.date=pDate;
   this.appdataService.sampleDate=this.date + "-" + this.loadMonth + "-" + this.loadYear;
    var dateSelected =this.date + "-" + this.SelectedMonthIndex + "-" + this.loadYear;
   console.log("Shubh dateSelected", dateSelected);
   console.log("Completed date",this.appdataService.sampleDate);
  	this.appdataService.calenderShow = false;
    this.appdataService.lodingIconShow = true;
   this.date=pDate;
   this.appdataService.sampleDate=this.date + "-" + this.loadMonth + "-" + this.loadYear;
   this.appdataService.calenderShow = false;
   
   let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});       
   let vOptions = new RequestOptions({"headers": vHeaders});
   var dBodyDataMaking = null;
   
   if(dateSelected != undefined || dateSelected != null)
        {
         
   if(dateSelected != undefined || dateSelected != null) {  
           dBodyDataMaking = '?frmDate=' + dateSelected;
   }
   
   if(dateSelected != undefined || dateSelected != null) {
           dBodyDataMaking += '&toDate=' + dateSelected;
          }
    console.log("date send date", dBodyDataMaking);
    
    this.http.get(this.appdataService.ip + 'attendence/search' + dBodyDataMaking, {"headers": vHeaders})
              .map(res => res.json()).subscribe(res=> {
                this.searchResponseData = res;
                console.log("Date click response", this.searchResponseData);
                this.appdataService.userData.userRes = this.searchResponseData; 
                console.log("Date response after filtered",  this.appdataService.userData.userRes);
                this.appdataService.lodingIconShow = false;
              },err=>{
            console.log("err", err);
                this.appdataService.showAlert(err);
                /*this.appdataService.errorMsg = "Check Internet Connection";*/
                this.appdataService.lodingIconShow = false;  
            
          });
    
   }else{
           this.appdataService.lodingIconShow = false;
           this.appdataService.errorMsg = "Select data first";
            // alert("Select data first");
        }
   
   
   
   
   
 }
 
   
  login(){
 		
	}

  signOut() {
     this.appdataService.userData.username = '';
     this.route.navigateByUrl('');
  }
 

  getAddAttendance(){
   
    this.http.get(this.appdataService.ip+'layout/departments')
          .map(res => res.json()).subscribe(res=> {
            this.appdataService.addAttendanceDept = res[0];
            console.log("this.appdataService.addAttendanceDept", this.appdataService.addAttendanceDept);
            /*console.log("log data", JSON.stringify(this.loginData));*/
      },err=>{
        this.appdataService.showAlert(err);
        console.log("err", err);
      }); 
    
 }
  
	searchData(){
          this.appdataService.lodingIconShow = true;
          let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});       
          
          let vOptions = new RequestOptions({"headers": vHeaders});
      
          /* this.http.get('../assets/loginData.json')*/
          this.http.get(this.appdataService.ip + 'attendence/ui', {"headers": vHeaders})
              .map(res => res.json())
              .subscribe(res=> {
                this.searchResponseData = res;
                console.log("Search Data", this.searchResponseData);
               
                /*console.log("log data", JSON.stringify(this.loginData));*/
               for(var i=0; i<this.searchResponseData.length; i++) { 
                 for(var y=0; y<this.searchResponseData[i].doctorsInDepartment.length; y++){
                    /*console.log("doctorName", this.searchResponseData[i].doctorsInDepartment.length); */
                    this.doctorsDepartName.push({dName:this.searchResponseData[i].doctorsInDepartment[y].doctorName, dId:this.searchResponseData[i].doctorsInDepartment[y].docId, dDesig:this.searchResponseData[i].doctorsInDepartment[y].designation});
                   }
               }
               /*for(var i=0; i<this.searchResponseData.length; i++) {
                for(var j=0; j<this.searchResponseData[i].day.length; j++) {
                    for(var k=0; k<this.searchResponseData[i].day[j].shifts.length; k++) {                       
                        for(var l=0; l<this.searchResponseData[i].day[j].shifts[k].rooms.length; l++) {
                         this.roomData.push({rId:this.searchResponseData[i].day[j].shifts[k].rooms[l].roomId, rName:this.searchResponseData[i].day[j].shifts[k].rooms[l].roomName});  
 
                        }
                    }
                }
              }*/
            this.appdataService.lodingIconShow = false;
          },err=>{
            this.appdataService.lodingIconShow = false;
            console.log("err", err);
            this.appdataService.showAlert(err);
            this.appdataService.errorMsg = "Search data error";
            //alert("Search data error");
          }); 
 
      }
 
      departSelectedId(pDataObj) {
        console.log("departSelectedId ", pDataObj);
        this.roomData = [];
        for(var i=0; i<this.searchResponseData.length; i++) {
           for(var y=0; y<this.searchResponseData[i].doctorsInDepartment.length; y++){ 
             this.doctorsDepartName.push({"dName":this.searchResponseData[i].doctorsInDepartment[y].doctorName, "dId":this.searchResponseData[i].doctorsInDepartment[y].doctorName, "dDesig":this.searchResponseData[i].doctorsInDepartment[y].designation});
            /*console.log("doctorId Shubh",  this.doctorsDepartName);*/
           }
             if(this.searchResponseData[i].deptId == pDataObj) {
             for(var j=0; j<this.searchResponseData[i].day.length; j++) {
               for(var k=0; k<this.searchResponseData[i].day[j].shifts.length; k++) {
                 for(var l=0; l<this.searchResponseData[i].day[j].shifts[k].rooms.length; l++) {
                   this.roomData.push({"rId":this.searchResponseData[i].day[j].shifts[k].rooms[l].roomId, "rName":this.searchResponseData[i].day[j].shifts[k].rooms[l].roomName});

                 }
               }
             }
          }
        }
      }
 
    goToHome(pFromDate, pToDate){
        this.appdataService.lodingIconShow = true;
        this.route.navigateByUrl("/home");
        
        if(pFromDate != undefined || pFromDate != null || 
          pToDate != undefined || pToDate != null)
        {
             /* Custom Date Format  */
            if(pFromDate != undefined || pFromDate != null) {
              var fromDate = pFromDate;
              var fromDateCustom = fromDate.split("-",3);
              var customFromDate = fromDateCustom[2] + "-" + fromDateCustom[1] + "-" + fromDateCustom[0];
              /*console.log("shubh from full", customFromDate);*/
            }
            /*var customFromDate = this.date + "-" + this.month + "-" + this.year;*/ 
            if(pToDate != undefined || pToDate != null) {
              var toDate = pToDate;
              var toDateCustom = toDate.split("-",3);  
              var customToDate = toDateCustom[2] + "-" + toDateCustom[1] + "-" + toDateCustom[0];
              /*console.log("shubh to full", customToDate);*/ 
            }
            
            let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});       
            let vOptions = new RequestOptions({"headers": vHeaders});
        
            var dBodyDataMaking = null;
        
            if(customFromDate != undefined || customFromDate != null) {  
               dBodyDataMaking = '?frmDate=' + customFromDate;
              }
            if(customToDate != undefined || customToDate != null) {
               dBodyDataMaking += '&toDate=' + customToDate;
              }
              
              this.http.get(this.appdataService.ip + 'attendence/search' + dBodyDataMaking, {"headers": vHeaders})
              .map(res => res.json()).subscribe(res=> {
                this.searchResponseData = res;
                console.log("Search click response", this.searchResponseData);
                this.appdataService.userData.userRes = this.searchResponseData; 
                console.log("Search response after filtered",  this.appdataService.userData.userRes);
                this.appdataService.sampleDate = this.appdataService.fullDate;
                
                this.appdataService.lodingIconShow = false;
              },err=>{
                  console.log("err", err);
                  this.appdataService.showAlert(err);
                  /*this.appdataService.errorMsg = "Check Internet Connection";*/
          }); 
        
        }else{
           this.appdataService.lodingIconShow = false;
           this.appdataService.errorMsg = "Select data first";
            // alert("Select data first");
        }
    }    
        
 
      SearchFilter(pFromDate, pToDate, pDeptData, pRoom, pDoctor) {
        this.appdataService.lodingIconShow = true;
        this.route.navigateByUrl("/home");
        /*console.log("search Filter pFromDate", pFromDate);
        console.log("search Filter pToDate", pToDate);
        console.log("search Filter pDeptData", pDeptData);
        console.log("search Filter pRoom", pRoom);
        console.log("search Filter pDoctor", pDoctor);*/
       
       if(pFromDate != undefined || pFromDate != null || 
          pToDate != undefined || pToDate != null || 
          pDeptData != undefined || pDeptData != null ||
          pRoom != undefined || pRoom != null ||
          pDoctor != undefined || pDoctor != null)
        {
         
        /* Custom Date Format  */
        if(pFromDate != undefined || pFromDate != null) {
          var fromDate = pFromDate;
          var fromDateCustom = fromDate.split("-",3);
          var customFromDate = fromDateCustom[2] + "-" + fromDateCustom[1] + "-" + fromDateCustom[0];
          /*console.log("shubh from full", customFromDate);*/
        }
        
        /*var customFromDate = this.date + "-" + this.month + "-" + this.year;*/ 
        if(pToDate != undefined || pToDate != null) {
          var toDate = pToDate;
          var toDateCustom = toDate.split("-",3);  
          var customToDate = toDateCustom[2] + "-" + toDateCustom[1] + "-" + toDateCustom[0];
          /*console.log("shubh to full", customToDate);*/ 
        }
        
         let vHeaders=new Headers({"Authorization": "Basic " + btoa(this.appdataService.userData.username+ ":" + this.appdataService.userData.password), "Accept": "application/json", "Content-Type": "application/json", "Access-Control-Allow-Origin":"*"});       
          
         let vOptions = new RequestOptions({"headers": vHeaders});
         
         var dBodyDataMaking = null;
        
        if(customFromDate != undefined || customFromDate != null) {  
           dBodyDataMaking = '?frmDate=' + customFromDate;
 
          }
        if(customToDate != undefined || customToDate != null) {
           dBodyDataMaking += '&toDate=' + customToDate;
          }
        if(pDeptData != undefined || pDeptData != null) {
            dBodyDataMaking += '&dept_id=' + pDeptData;
           }
        if(pRoom != undefined || pRoom != null) {
             dBodyDataMaking += '&room_id=' + pRoom;
            }
        if(pDoctor != undefined || pDoctor != null) {
             dBodyDataMaking += '&doc_id=' + pDoctor;
            }  
       
        /*console.log("dBodyDataMaking", dBodyDataMaking);*/
       
          /*var dBody= '?frmDate=' + customFromDate + '&toDate=' + customToDate + '&dept_id=' + pDeptData + '&room_id=' + pRoom + '&doc_id=' + pDoctor;*/
         
         /* {
                     "frmDate": customFromDate,
                     "toDate": customToDate,
                     "dept_id": pDeptData,
                     "room_id": pRoom,
                     "doc_id": pDoctor
                    
             };*/   
            /*console.log("dBody", this.appdataService.ip + '/DashBoard/attendence/search' + dBody);*/
       
          this.http.get(this.appdataService.ip + 'attendence/search' + dBodyDataMaking, {"headers": vHeaders})
              .map(res => res.json()).subscribe(res=> {
                this.searchResponseData = res;
                console.log("Search click response", this.searchResponseData);
                this.appdataService.userData.userRes = this.searchResponseData; 
                console.log("Search response after filtered",  this.appdataService.userData.userRes);
                this.appdataService.lodingIconShow = false;
              },err=>{
            console.log("err", err);
                this.appdataService.showAlert(err);
                /*this.appdataService.errorMsg = "Check Internet Connection";*/
            
          }); 
           
           
        }else{
            this.appdataService.lodingIconShow = false;
            this.appdataService.errorDiv = true;
            this.appdataService.errorMsg = "Select data first";
            /*alert("Select data first");*/
            
            this.appdataService.showAlert("Select data");
            
        }
        
       
       
     }
  
      issueLog() {
          this.appdataService.lodingIconShow = true;
         /* this.route.navigateByUrl("/Issuelog");*/
      }
  
}